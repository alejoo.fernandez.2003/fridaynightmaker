This software is not mine. So if you plan using it for profit, contact the game creators here: https://kingamescreator.itch.io/friday-night-maker.

Credits:

-Programming and design: KinGamesCreator.

-Logo (Menú): Nobakou_art.

-Translation: Lisanicolas366.

-Players defaults: GreenRetroman

-Background Default: T´K.

-Music Default: "Ho" (Alejandro Maciá) and "Superiority" (Nabo).

-Graphics of default levels: GreenRetroman & T'K (Superiority);  MoeDev (Ho).

